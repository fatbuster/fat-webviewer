# osimiswebviewer

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.14.0.

## Install

```sh
bower install --save OsimisWebViewer
```

note: incompatible w/ angular != 1.5.0-rc.0 - check your bower.json on strange bower behaviors.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
